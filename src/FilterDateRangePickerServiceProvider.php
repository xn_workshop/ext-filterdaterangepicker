<?php

namespace Xn\FilterDateRangePicker;

use Xn\Admin\Admin;
use Illuminate\Support\ServiceProvider;

class FilterDateRangePickerServiceProvider extends ServiceProvider
{
    /**
     * {@inheritdoc}
     */
    public function boot(FilterDateRangePickerExtension $extension)
    {
        if (! FilterDateRangePickerExtension::boot()) {
            return ;
        }

        $this->loadViewsFrom(resource_path('views')."/vendor/xn/filterdaterangepicker/views", $extension->name);

        $this->registerPublishing($extension);

        Admin::booting(function () {
            Admin::js('vendor/laravel-admin-ext/filterdaterangepicker/daterangepicker/moment.min.js');
            Admin::js('vendor/laravel-admin-ext/filterdaterangepicker/daterangepicker/moment-timezone-with-data.min.js');
            Admin::js('vendor/laravel-admin-ext/filterdaterangepicker/daterangepicker/daterangepicker.js');
            Admin::css('vendor/laravel-admin-ext/filterdaterangepicker/daterangepicker/daterangepicker.css');
        });
    }

    /**
     * Register the package's publishable resources.
     *
     * @return void
     */
    protected function registerPublishing(FilterDateRangePickerExtension $extension)
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([__DIR__.'/../resources/lang' => resource_path('lang')], $extension->name);
            $this->publishes([__DIR__.'/../resources/views' => resource_path('views')."/vendor/xn/filterdaterangepicker/views"], $extension->name);
            $this->publishes([__DIR__.'/../resources/assets' => public_path('vendor/laravel-admin-ext/filterdaterangepicker')], $extension->name);
            $this->publishes([__DIR__.'/config.php' => config_path('daterangepicker.php')], $extension->name);
        }
    }
}
