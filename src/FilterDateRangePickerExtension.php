<?php

namespace Xn\FilterDateRangePicker;

use Xn\Admin\Extension;

class FilterDateRangePickerExtension extends Extension
{
    public $name = 'laravel-admin-filterdaterangepicker';

    public $views = __DIR__.'/../resources/views';

    public $assets = __DIR__.'/../resources/assets';
}
